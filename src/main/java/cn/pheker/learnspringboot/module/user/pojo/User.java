package cn.pheker.learnspringboot.module.user.pojo;

/**
 * <pre>
 * @Author cn.pheker
 * @Date 2019/3/30 0:39
 * @Email 1176479642@qq.com
 * @Version v1.0.0
 * @Desc
 *
 * </pre>
 */
public class User {
    
    private String name;
    private int age;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    
    public static UserBuilder builder(){
        return UserBuilder.anUser();
    }
    public static final class UserBuilder {
        private String name;
        private int age;
        
        private UserBuilder() {
        }
        
        private static UserBuilder anUser() {
            return new UserBuilder();
        }
        
        public UserBuilder name(String name) {
            this.name = name;
            return this;
        }
        
        public UserBuilder age(int age) {
            this.age = age;
            return this;
        }
        
        public User build() {
            User user = new User();
            user.age = this.age;
            user.name = this.name;
            return user;
        }
    }
}
