package cn.pheker.learnspringboot.module.user.controller;

import cn.pheker.learnspringboot.common.annotation.CalcTime;
import cn.pheker.learnspringboot.module.user.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * @Author cn.pheker
 * @Date 2019/3/30 0:36
 * @Email 1176479642@qq.com
 * @Version v1.0.0
 * @Desc
 *
 * </pre>
 */
@RestController
@CalcTime
public class UserController {
    
    private Logger log = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    Map<String, String> urls;
    
    @RequestMapping("/")
    public String index() {
        return "Hello World!";
    }
    
    @RequestMapping("/user")
    public User defaultUser() {
        User itslulo = User.builder().name("itslulo-安洁").build();
        log.info("user -> {}", itslulo);
        return itslulo;
    }
    
    @CalcTime(managerName = "magicTimeManager", verbose = true)
    @RequestMapping("urls")
    public Map<String, String> urls(){
        try {
            TimeUnit.MILLISECONDS.sleep(1248);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return urls;
    }
}
