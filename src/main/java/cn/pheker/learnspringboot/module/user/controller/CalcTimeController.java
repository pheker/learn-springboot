package cn.pheker.learnspringboot.module.user.controller;

import cn.pheker.learnspringboot.common.annotation.CalcTime;
import cn.pheker.learnspringboot.common.annotation.TimeInfo;
import cn.pheker.learnspringboot.common.util.UtilString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.List;

/**
 * <pre>
 * @Author cn.pheker
 * @Date 2019/3/31 14:34
 * @Email 1176479642@qq.com
 * @Version v1.0.0
 * @Desc
 *
 * </pre>
 */

@RestController
public class CalcTimeController {
    
    @Autowired
    TimeInfo.CalcTimeManager mtm;
    
    @RequestMapping("/times/list")
    public String list() {
    
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HH:mm:ss_SSS");
        
        StringBuilder list = new StringBuilder();
        list.append("<style>" +
                "table.gridtable {" +
                    "font-family: verdana,arial,sans-serif;" +
                    "font-size: 11px;" +
                    "color: #333333;" +
                    "border-width: 1px;" +
                    "border-color: #666666;" +
                    "border-collapse: collapse;" +
                "}" +
                "table.gridtable th {" +
                    "border-width: 1px;" +
                    "padding: 8px;" +
                    "border-style: solid;" +
                    "border-color: #666666;" +
                    "background-color: #009688;" +
                "}" +
                "table.gridtable td {" +
                    "border-width: 1px;" +
                    "padding: 8px;" +
                    "border-style: solid;" +
                    "border-color: #666666;" +
                    "background-color: #ffffff;" +
                "}" +
                "</style>");
        
        list.append("SUM: " + mtm.getSum() + ", SIZE: " + mtm.size() + ", QPS: "+mtm.getSumQps()+"<br/>");
        list.append("<table class='gridtable'>" +
                "<thead>" +
                    "<tr>" +
                        "<th class='api'>api</th>" +
                        "<th class='count'>count</th>" +
                        "<th class='firstTime'>firstTime</th>" +
                        "<th class='lastTime'>lastTime</th>" +
                        "<th class='max'>max</th>" +
                        "<th class='min'>min</th>" +
                        "<th class='avg'>avg</th>" +
                        "<th class='interval'>interval</th>" +
                        "<th class='startTime'>startTime</th>" +
                        "<th class='endTime'>endTime</th>" +
                        "<th class='cost'>cost</th>" +
                        "<th class='QPS'>QPS</th>" +
                    "</tr>" +
                "</thead>" +
            "<tbody>");
        Enumeration<TimeInfo> elements = mtm.elements();
        while (elements.hasMoreElements()) {
            TimeInfo time = elements.nextElement();
            String row = UtilString.format(
                    "<tr>" +
                            "<td class='api'>{}</td>" +
                            "<td class='count'>{}</td>" +
                            "<td class='firstTime'>{}</td>" +
                            "<td class='lastTime'>{}</td>" +
                            "<td class='max'>{}</td>" +
                            "<td class='min'>{}</td>" +
                            "<td class='avg'>{}</td>" +
                            "<td class='interval'>{}</td>" +
                            "<td class='startTime'>{}</td>" +
                            "<td class='endTime'>{}</td>" +
                            "<td class='cost'>{}</td>" +
                            "<td class='QPS'>{}</td>" +
                        "</tr>",
                    time.getMagicNameSimplified(),
                    time.getCount(),
                    time.isFirst() ? "" : sdf.format(time.getFirstTime()),
                    time.isFirst() ? "" : sdf.format(time.getLastTime()),
                    
                    time.getMaxCostTimeFormatted(),
                    time.getMinCostTimeFormatted(),
                    time.getAvgCostTimeFormatted(),
                    time.getAvgIntervalTimeFormatted(),
        
                    sdf.format(time.getThisTime()),
                    sdf.format(time.getThisEndTime()),
                    time.getThisCostTimeFormatted(),
                    time.getQps()
            );
            list.append(row);
        }
        list.append(
                "</tbody>" +
                "</table>");
        return list.toString();
    }
    
    @RequestMapping("/times/find")
    public TimeInfo[] find(){
        return (TimeInfo[]) mtm.values().toArray(new TimeInfo[0]);
    }
    
    @RequestMapping("/times/find/{key}")
    public List<TimeInfo> find(@PathVariable("key") @Nullable String key){
        return mtm.find(key);
    }
}
