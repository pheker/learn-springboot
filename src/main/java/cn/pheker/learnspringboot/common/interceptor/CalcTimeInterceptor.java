package cn.pheker.learnspringboot.common.interceptor;

import cn.pheker.learnspringboot.common.annotation.CalcTime;
import cn.pheker.learnspringboot.common.annotation.TimeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * <pre>
 * @Author  cn.pheker
 * @Date    2019/3/30 19:49
 * @Email   1176479642@qq.com
 * @Version v1.0.0
 * @Desc    统计耗时拦截器
 *
 * </pre>
 */

public class CalcTimeInterceptor implements HandlerInterceptor, BeanFactoryAware {
    
    Logger log = LoggerFactory.getLogger(CalcTimeInterceptor.class);
    
    /**
     * 作用范围：全局
     */
    private boolean global = false;
    private boolean globalVerbose = false;
    private BeanFactory beanFactory;

    public CalcTimeInterceptor() {
    }
    
    public CalcTimeInterceptor(boolean global) {
        this.global = global;
    }
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Class<?> beanType = handlerMethod.getBeanType();
            Method method = handlerMethod.getMethod();
            // 方法或类上存在@CalcTime注解
            if (global || method.isAnnotationPresent(CalcTime.class) || beanType.isAnnotationPresent(CalcTime.class)) {
                Date startDate = new Date();
                request.setAttribute("orz_time_start", startDate);
            }
        }
        return true;
    }
    
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {
        
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
                // 方法或类上存在@CalcTime注解
                CalcTime calctime = getCalcTime(handlerMethod);
                if (global || calctime != null) {
                    Date startDate = (Date) request.getAttribute("orz_time_start");
                    Date endDate = new Date();
                    String magicName = handlerMethod.getBeanType().getName() + "-" + handlerMethod.getMethod().getName();
                    TimeInfo timeInfo = bringIntoManager(calctime, TimeInfo.instance(magicName, startDate, endDate));
                    showMagicTime(timeInfo, calctime==null? globalVerbose:calctime.verbose());
                }
        }
    }
    
    /**
     * 显示时间
     * @param timeInfo 耗时记录
     * @param verbose 详细
     */
    private void showMagicTime(TimeInfo timeInfo, boolean verbose) {
        if (verbose) {
            log.info(timeInfo.toString());
        }else {
            log.info("[{}]: {}", timeInfo.getMagicNameSimplified(), timeInfo.getThisCostTimeFormatted());
        }
    }
    
    /**
     * 纳入管理中
     * @param calcTime @CalcTime注解
     * @param timeInfo 耗时信息
     */
    private TimeInfo bringIntoManager(CalcTime calcTime, TimeInfo timeInfo) {
        // 1.find manager
        TimeInfo.CalcTimeManager mtm = null;
       
        String managerName = getManagerName(calcTime);
    
        //@CalcTime(magicPortal="xxx") 存在管理器
        if (!StringUtils.isEmpty(managerName)) {
            Object container = beanFactory.getBean(managerName);
            if (container instanceof TimeInfo.CalcTimeManager) {
                mtm = (TimeInfo.CalcTimeManager) container;
            }
        }else{
            Class<? extends TimeInfo.CalcTimeManager> clzz = calcTime == null?
                    TimeInfo.GenericCalcTimeManager.class:calcTime.managerClass();
            mtm = beanFactory.getBean(clzz);
        }
        
        // 2.record into manager
        String magicName = timeInfo.getMagicName();
        //是否存在对应的耗时记录
        TimeInfo timeInfoLast = mtm.get(magicName);
        if (timeInfoLast == null) {
            mtm.put(magicName, timeInfo);
        }else{//已存在则合并到一起
            timeInfo = timeInfoLast.andThen(timeInfo);
        }
        mtm.increment();
        
        return timeInfo;
    }
    
    private String getManagerName(CalcTime calcTime) {
        if(calcTime == null) return null;
        String managerName = calcTime.value();
        if (StringUtils.isEmpty(managerName)) {
            managerName = calcTime.managerName();
        }
        return managerName;
    }
    
    /**
     * 查找注解
     * @return          方法或类的注解
     */
    private CalcTime getCalcTime(HandlerMethod handlerMethod) {
        CalcTime calctime = null;
        CalcTime[] calctimes = handlerMethod.getMethod().getDeclaredAnnotationsByType(CalcTime.class);
        if (calctimes != null && calctimes.length > 0) {
            calctime = calctimes[0];
        }else{
            calctimes = handlerMethod.getBeanType().getDeclaredAnnotationsByType(CalcTime.class);
            if (calctimes != null && calctimes.length > 0) {
                calctime = calctimes[0];
            }
        }
        return calctime;
    }
    
    
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
    
    public void setGlobal(boolean global) {
        this.global = global;
    }
}
