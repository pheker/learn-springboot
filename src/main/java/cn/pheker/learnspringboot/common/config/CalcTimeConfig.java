package cn.pheker.learnspringboot.common.config;

import cn.pheker.learnspringboot.common.annotation.TimeInfo;
import cn.pheker.learnspringboot.common.interceptor.CalcTimeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <pre>
 * @Author  cn.pheker
 * @Date    2019/3/30 0:59
 * @Email   1176479642@qq.com
 * @Version v1.0.0
 * @Desc    CalcTime配置器
 *
 * </pre>
 */
@Configuration
public class CalcTimeConfig implements WebMvcConfigurer {
    
    CalcTimeInterceptor calcTimeInterceptor = new CalcTimeInterceptor(true);
    
    @Bean
    public CalcTimeInterceptor calcTimeInterceptor(){
        return calcTimeInterceptor;
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        calcTimeInterceptor.setGlobal(true);
        registry.addInterceptor(calcTimeInterceptor).addPathPatterns("/**");
    }
    
    @Bean
    public TimeInfo.CalcTimeManager magicTimeManager(){
        return new TimeInfo.GenericCalcTimeManager();
    }
}
