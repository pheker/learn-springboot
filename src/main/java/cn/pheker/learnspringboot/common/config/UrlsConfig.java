package cn.pheker.learnspringboot.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * @Author cn.pheker
 * @Date 2019/3/30 10:53
 * @Email 1176479642@qq.com
 * @Version v1.0.0
 * @Desc
 *
 * </pre>
 */

@Configuration
public class UrlsConfig {
    
    @Value("${code.author}")
    private String codeAuthor;
    
    @Value("${url.baidu}")
    private String baiduUrl;
    
    @Value("${url.weather}")
    private String weatherUrl;
    
    @Bean
    public Map<String, String> urls(){
        Map<String, String> urls = new HashMap<>();
        urls.put("baiduUrl", baiduUrl);
        urls.put("weatherUrl", weatherUrl);
        urls.put("codeAuthor", codeAuthor);
        return urls;
    }
}
