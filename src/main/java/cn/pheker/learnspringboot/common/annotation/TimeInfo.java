package cn.pheker.learnspringboot.common.annotation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <pre>
 * @Author  cn.pheker
 * @Date    2019/3/31 0:50
 * @Email   1176479642@qq.com
 * @Version v1.0.0
 * @Desc    存储关于访问的相关信息
 *
 * </pre>
 */
public class TimeInfo implements Comparable<TimeInfo>{
//    firstTime, lastTime, maxCostTime, minCostTime, avg, thisTime, times
    /**
     * 名字
     */
    private final String magicName;
    
    /**
     * 第一次访问时间
     */
    Date firstTime;
    /**
     * 最后一次访问时间
     */
    Date lastTime;
    
    /**
     * 最大耗时
     */
    long maxCostTime;
    /**
     * 最小耗时
     */
    long minCostTime;
    /**
     * 访问次数
     */
    int count;
    /**
     * 本次访问时间
     */
    Date thisTime;
    
    /**
     * 本次访问结束时间
     */
    Date thisEndTime;
    /**
     * 本次耗时
     */
    long thisCostTime;
    
    private TimeInfo(String magicName, Date thisTime, Date thisEndTime) {
        this.magicName = magicName;
        this.goAhead(thisTime, thisEndTime);
    }
    
    private void goAhead(Date thisTime, Date thisEndTime) {
        lastTime = this.thisTime;
        if (firstTime == null) {
            firstTime = lastTime;
        }
        
        this.thisTime = thisTime;
        this.thisEndTime = thisEndTime;
        this.thisCostTime = thisEndTime.getTime() - thisTime.getTime();
        if (thisCostTime > maxCostTime) {
            maxCostTime = thisCostTime;
        } else if (minCostTime==0 || thisCostTime < minCostTime) {
            minCostTime = thisCostTime;
        }
        count++;
    }
    
    public static TimeInfo instance(String magicName, Date thisTime, Date thisEndTime){
        return new TimeInfo(magicName, thisTime, thisEndTime);
    }
    
    public String getMagicName() {
        return magicName;
    }
    public String getMagicNameSimplified() {
        return simplifiyName(magicName);
    }
    
    public Date getFirstTime() {
        return firstTime;
    }
    
    public Date getLastTime() {
        return lastTime;
    }
    
    public long getMaxCostTime() {
        return maxCostTime;
    }
    
    public String getMaxCostTimeFormatted() {
        return dateDiff(maxCostTime);
    }
    
    public long getMinCostTime() {
        return minCostTime;
    }
    
    public String getMinCostTimeFormatted() {
        return dateDiff(minCostTime);
    }
    
    /**
     * 访问总次数
     * @return
     */
    public int getCount() {
        return count;
    }
    
    /**
     * 本次开始时间
     * @return
     */
    public Date getThisTime() {
        return thisTime;
    }
    
    /**
     * 本次结束时间
     * @return
     */
    public Date getThisEndTime() {
        return thisEndTime;
    }
    
    /**
     * 本次耗时
     * @return
     */
    public long getThisCostTime() {
        return thisCostTime;
    }
    
    public String getThisCostTimeFormatted(){
        return dateDiff(thisCostTime);
    }
    
    /**
     * 平均耗时
     * @return
     */
    public long getAvgCostTime(){
        return (maxCostTime + minCostTime) / 2;
    }
    
    public String getAvgCostTimeFormatted(){
        return dateDiff(getAvgCostTime());
    }
    
    /**
     * 平均间隔
     * @return
     */
    public long getAvgIntervalTime(){
        return isFirst() ? 0 : (thisTime.getTime() - firstTime.getTime()) / count;
    }
    
    /**
     * QPS
     * @return
     */
    public float getQps(){
        return isFirst() ? 0 : new BigDecimal(1000 * count)
                .divide(new BigDecimal(new Date().getTime() - firstTime.getTime()), BigDecimal.ROUND_HALF_UP)
                .setScale(2)
                .floatValue();
    }
    
    public String getAvgIntervalTimeFormatted(){
        long ms = getAvgIntervalTime();
        
        long s = ms/1000;
        ms %= 1000;
        
        long m = s/60;
        s %= 60;
        
        long h = m/60;
        m %= 60;
        return String.format("%d.%02d.%02d_%03d", h, m, s, ms);
    }
    
    // TODO getAvgIntervalTimeFormatted
    
    public boolean isFirst(){
        return firstTime == null || lastTime == null;
    }
    
    public String toStringRow(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HH:mm:ss_SSS");
        String max = getMaxCostTimeFormatted(), min = getMinCostTimeFormatted(), avg = getAvgCostTimeFormatted();
        return String.format(
                        "[%s]: %06d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
                getMagicNameSimplified(),
                getCount(),
                isFirst() ? "\t\t\t\t\t" : sdf.format(getFirstTime()),
                isFirst() ? "\t\t\t\t\t" : sdf.format(getLastTime()),
                max +(max.length()<4? "\t":""),
                min +(min.length()<4? "\t":""),
                avg +(avg.length()<4? "\t":""),
                getAvgIntervalTimeFormatted(),
            
                sdf.format(getThisTime()),
                sdf.format(getThisEndTime()),
                getThisCostTimeFormatted());
    }
    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HH:mm:ss_SSS");
        String max = getMaxCostTimeFormatted(), min = getMinCostTimeFormatted(), avg = getAvgCostTimeFormatted();
        return String.format("\n\n"+
                "+------>>>  [ %s ]\n" +
                "| count\t\tfirstTime\t\t\t\tlastTime\t\t\t\tmax\t\tmin\t\tavg\t\tinterval\tstartTime\t\t\t\tendTime\t\t\t\t\tcost\n" +
                "| %06d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" +
                "+------>>>\n", getMagicNameSimplified(),
                getCount(),
                isFirst() ? "\t\t\t\t\t" : sdf.format(getFirstTime()),
                isFirst() ? "\t\t\t\t\t" : sdf.format(getLastTime()),
                max +(max.length()<4? "\t":""),
                min +(min.length()<4? "\t":""),
                avg +(avg.length()<4? "\t":""),
                getAvgIntervalTimeFormatted(),
            
                sdf.format(getThisTime()),
                sdf.format(getThisEndTime()),
                getThisCostTimeFormatted());
    }
    
    /**
     * 简化类路径
     * @param name
     * @return
     */
    public static String simplifiyName(String name){
        if(name.indexOf("/") > -1){
            name = name.replaceAll("/+", "");
        }
        if (name.startsWith("[.]") || name.endsWith("[.]")) {
            name = name.replace("^[.]+|[.]+$", "");
        }
        String[] split = name.split("\\.");
        int max;
        if ((max = split.length) > 4) {
            StringBuilder sb = new StringBuilder(split[0] + "." + split[1]);
            max -= 2;
            for (int i = 2; i < max; i++) {
                sb.append('.');
                sb.append(split[i].substring(0, 1));
            }
            return sb.toString()+"."+split[max]+"."+split[max+1];
        }
        return name;
    }
    
    /**
     * 计算并格式化时间差
     *   ms     34
     * s.ms 23.056
     * @param timeStart 开始时间
     * @param timeEnd   结束时间
     * @return
     */
    public static String dateDiff(Date timeStart, Date timeEnd){
        return dateDiff(timeEnd.getTime() - timeStart.getTime());
    }
    
    /**
     * 格式化时间差
     *   ms     34
     * s.ms 23.056
     * @return
     */
    public static String dateDiff(long ms){
        if (ms < 0) {
            ms = - ms;
        }
        long s = ms / 1000;
        ms = ms % 1000;
        return s > 0 ? String.format("%d.%03d", s, ms) : String.format("%d", ms);
    }
    
    public TimeInfo andThen(TimeInfo timeInfo) {
        goAhead(timeInfo.getThisTime(), timeInfo.getThisEndTime());
        return this;
    }
    
    @Override
    public int compareTo(TimeInfo o) {
        float v = this.getQps() - o.getQps();
        return v > 0 ? 1 : v < 0 ? -1 : (this.count - o.count);
    
    }
    
    
    /**
     * 耗时统计管理器-接口
     */
    public interface CalcTimeManager {
        
        void put(String key, TimeInfo timeInfo);
    
        TimeInfo get(String methodName);
        
        Enumeration<TimeInfo> elements();
        
        int size();
        
        void increment();
        
        int incrementAndGet();
    
        int getSum();
    
        List<TimeInfo> find(String key);
    
        List<TimeInfo> find(Class beanType);
    
        Collection values();
    
        float getSumQps();
    }
    
    /**
     *耗时统计管理器-通用实现
     */
    public static class GenericCalcTimeManager extends ConcurrentHashMap
            implements CalcTimeManager, Comparator<TimeInfo> {
    
        private AtomicInteger sum = new AtomicInteger(0);
    
        @Override
        public void put(String key, TimeInfo timeInfo) {
            super.put(key, timeInfo);
        }
    
        @Override
        public TimeInfo get(String key) {
            return (TimeInfo) super.get(key);
        }
    
        @Override
        public Enumeration<TimeInfo> elements() {
            return super.elements();
        }
    
        @Override
        public int incrementAndGet() {
            return sum.incrementAndGet();
        }
        
        public void increment(){
            incrementAndGet();
        }
        
        @Override
        public int getSum(){
            return sum.get();
        }
    
        @Override
        public List<TimeInfo> find(String key) {
            List<TimeInfo> list = new ArrayList<>(4);
            Set<Entry<String, TimeInfo>> set = super.entrySet();
            Iterator<Entry<String, TimeInfo>> ite = set.iterator();
            while (ite.hasNext()) {
                Entry<String, TimeInfo> next = ite.next();
                if (next.getKey().contains(key)) {
                    list.add(next.getValue());
                }
            }
            return list;
        }
    
        @Override
        public List<TimeInfo> find(Class beanType) {
            return find(beanType.getName());
        }
    
        @Override
        public Collection values() {
            return super.values();
        }
    
        @Override
        public float getSumQps() {
            Date firstTime = null, lastTime = null;
            Enumeration<TimeInfo> elements = elements();
            while (elements.hasMoreElements()) {
                TimeInfo timeInfo = elements.nextElement();
                if(firstTime == null) {
                    firstTime = timeInfo.getFirstTime();
                }
                if (firstTime != null && timeInfo.firstTime != null
                    && firstTime.after(timeInfo.firstTime)) {
                    firstTime = timeInfo.firstTime;
                }
            }
            if (firstTime == null) {
                return 0;
            }
            return new BigDecimal(1000 * sum.get())
                    .divide(new BigDecimal(new Date().getTime() - firstTime.getTime()), BigDecimal.ROUND_HALF_UP)
                    .setScale(2, BigDecimal.ROUND_UP)
                    .floatValue();
        }
    
        @Override
        public int compare(TimeInfo o1, TimeInfo o2) {
            return o1.compareTo(o2);
        }
    }
}
