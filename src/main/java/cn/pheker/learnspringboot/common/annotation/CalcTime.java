package cn.pheker.learnspringboot.common.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * <pre>
 * @Author  cn.pheker
 * @Date    2019/3/30 19:21
 * @Email   1176479642@qq.com
 * @Version v1.0.0
 * @Desc    calculate elapsed time and statistic related information.
 * usage:
 *      default:
 *      @CalcTime
 *      @CalcTime(verbose = false)
 *      @CalcTime(managerClass = TimeInfo.GenericCalcTimeManager.class)
 *
 *      custom:
 *      @CalcTime(managerClass = YourCalcTimeManager.class)
 *      @CalcTime(managerClass = YourCalcTimeManager.class, verbose = true)
 *
 *      you also can specify specify the name of the subclass of CalcTimeManager,
 *          after you ensured it's been instantiated
 *      @CalcTime(managerName = "magicTimeManager")
 *      @CalcTime(managerName = "magicTimeManager", verbose = true)
 *
 *      otherwise, you can enable global option in configuration
 *      ```
 *      @Override
 *      public void addInterceptors(InterceptorRegistry registry) {
 *          calcTimeInterceptor.setGlobal(true);
 *          registry.addInterceptor(calcTimeInterceptor).addPathPatterns("/**");
 *      }
 *      actually, @CalcTime annotation has higher priorities.
 *      ```
 *
 * </pre>
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CalcTime {
    
    @AliasFor("managerName")
    String value() default "";
    
    @AliasFor("value")
    String managerName() default "";
    
    /**
     * 默认管理器类
     * @return
     */
    Class<? extends TimeInfo.CalcTimeManager> managerClass() default TimeInfo.GenericCalcTimeManager.class;
    
    boolean verbose() default false;
}
